package model.vo;

public class BusUpdateVO {
	int VehicleNo;
	int TripId;
	String RouteNo;
    String Direction;
	String Destination;
	String pattern;
	Double Latitude;
	Double Longitude;
	String RecordedTime;
	String routeMap;
	
public BusUpdateVO(int veicleNo, int tripId, String routeNo,String direction, String destination, String patt, double lat, double lon, String recordedTime, String rouMap)

{
	VehicleNo=veicleNo;
	TripId=tripId;
	RouteNo=routeNo;
	Direction=direction;
	Destination=destination;
	pattern=patt;
	Latitude=lat;
	Longitude=lon;
	RecordedTime=recordedTime;
	routeMap=rouMap;
		}
public int TripId()
{return TripId;
	}

public String RouteNo()
{return RouteNo;
	}
public String Destination()
{return Destination;
	}
public double Lat(){
	return Latitude;
}
public double Lon(){
	return Longitude;
}
public String RecordedTime()
{return RecordedTime;
	}
}

