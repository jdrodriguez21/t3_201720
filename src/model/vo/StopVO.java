package model.vo;

public class StopVO {

	private int Id;
	private String Name;
	private double Lat;
	private double Lon;
	private String ZoneId;
	
	public StopVO( int stopId,  String stopName, double stopLat, double stopLon, String stopZoneId)
	{
		Id= stopId;
		Name=stopName;
		Lat= stopLat;
		Lon= stopLon;
		ZoneId= stopZoneId;
	}
	
	public int getId()
	{
		return Id;
	}
	
	public String getName()
	{
		return Name;
	}
	public double getLat()
	{
		return Lat;
	}
	
	public double getLon()
	{
		return Lon;
	}
	
	public String getZone()
	{
		return ZoneId;
	}
}
