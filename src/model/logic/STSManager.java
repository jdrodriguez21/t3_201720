package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;




public class STSManager implements ISTSManager{
	
private Queue<BusUpdateVO> buses= new Queue<BusUpdateVO>();
private DoubleLinkedList<StopVO> stops;
private File archivo;
	@Override
	
	
	public IStack<StopVO> listStops(Integer tripID) {
		Stack<StopVO> res = new Stack<StopVO>();
		Queue<BusUpdateVO> temp = new Queue<BusUpdateVO>();
		
       while(!buses.isEmpty())
       {
    	   BusUpdateVO actB=buses.dequeue();
    	   temp.enqueue(actB);
    	   if(actB.TripId()==tripID)
    	   {
    		   double BLat = actB.Lat();
    		   double BLon = actB.Lon();
    		   Iterator<StopVO> iter=stops.iterator();
    		   boolean termino=false;
    		   while(iter.hasNext()&& !termino)
    		   {
    			   StopVO actSt= iter.next();
    			   double stLat=actSt.getLat();
    			   double stLon= actSt.getLon();
    			   if (getDistance(stLat, stLon, BLat, BLon)<70)
    			   {
    				   termino=true;
    				   res.push(actSt);
    			   }
    		   }
    	   }
       }
       return res;
	}
	
	@Override
	public void loadStops(String stopsFile)  {
	    stops= new DoubleLinkedList<StopVO>();
	
		archivo= new File(stopsFile);
		try {
				BufferedReader reader = new BufferedReader(new FileReader(archivo));
				String linea = reader.readLine();
				
				while(linea!= null)
				{
					String[] datos = linea.split(",");
					int Id= Integer.parseInt(datos[0]);
					String Name = datos[2];
					double Lat =Double.parseDouble(datos[4]);
					double Lon = Double.parseDouble(datos[5]);
					String Zone= datos[6];
					
					StopVO nueva = new StopVO(Id, Name, Lat, Lon, Zone);
					stops.addAtEnd(nueva);
					
					linea=reader.readLine();
				}
				reader.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println(e);
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        double latDistance = Math.toRadians(lat2-lat1);
        double lonDistance = Math.toRadians(lon2-lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = R * c;
         
        return distance;
 
    }

	@Override
	public void readBusUpdate(File rtFile) throws FileNotFoundException {
		
		BufferedReader reader = null;
	   
	        reader = new BufferedReader(new FileReader(rtFile));
	        Gson gson = new GsonBuilder().create();
	   
	        BusUpdateVO[] bus = gson.fromJson(reader, BusUpdateVO[].class);
	        
	        for(int i=0;i<bus.length;i++)
	        {
	        	buses.enqueue(bus[i]);
	        }
	}

}
