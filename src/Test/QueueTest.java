package Test;

import junit.framework.TestCase;
import model.data_structures.Queue;


public class QueueTest<E> extends TestCase{

	

private Queue <E> St;
private Queue<Integer> intt;
private E a;
private E be;
private E c;
private E d;

// Se compuebra que la pila funcione de forma generica//
public void setupEscenario1( )
{
 St = new Queue<E>();
 St.enqueue(a);
 St.enqueue(be);
 St.enqueue(c);
}
// se prueba el caso especifico de una pila de enteros//
public void setupEscenario2( )
{
	intt= new Queue<Integer>();
	intt.enqueue(1);
	intt.enqueue(2);
	intt.enqueue(3);
	intt.enqueue(4);
	intt.enqueue(5);
	intt.enqueue(6);
	
}
// se prueba que la pila s inicialize correctamente

public void testQueue()
{ 
 setupEscenario1( );
int s=St.Size();
 assertNotNull( "No debe ser nula ",St );
 assertEquals( "Size debe ser 3", 3, s);
 
 setupEscenario2( );
 int y= intt.Size();
 assertNotNull( "No debe ser nula ",intt );
 assertEquals( "Size debe ser 6", 6, y);
 	        	              }


public void testEnqueue()
{ setupEscenario1( );
St.enqueue(d);
int k= St.Size();
assertEquals( "Size debe ser 4", 4, k);

setupEscenario2();
intt.enqueue(55);
int j= intt.Size();
assertEquals( "Size debe ser 7", 7, j);

	
}
public void testDequeue()
{ setupEscenario1( );
E es=St.dequeue();

int k= St.Size();
assertEquals( "Size debe ser 2", 2, k);
assertEquals( "Debe ser d",d , es);

setupEscenario2();
int nk=intt.dequeue();
int j= intt.Size();
assertEquals( "Size debe ser 5", 5, j);
assertEquals( "Debe ser 1",1, nk);


int p=intt.dequeue();
assertEquals( "Debe ser 2",2 , p);

	
}
	
}
