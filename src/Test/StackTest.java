package Test;

import junit.framework.TestCase;
import model.data_structures.Stack;


public class StackTest<E> extends TestCase {




private Stack <E> St;
private Stack <Integer> intt;
private E a;
private E be;
private E c;
private E d;

// Se compuebra que la pila funcione de forma generica//
public void setupEscenario1( )
{
 St = new Stack<E>();
 St.push(a);
 St.push(be);
 St.push(c);
}
// se prueba el caso especifico de una pila de enteros//
public void setupEscenario2( )
{
	intt= new Stack<Integer>();
	intt.push(1);
	intt.push(2);
	intt.push(3);
	intt.push(4);
	intt.push(5);
	intt.push(6);
	
}
// se prueba que la pila s inicialize correctamente

public void testStack()
{ 
 setupEscenario1( );
int s=St.Size();
 assertNotNull( "No debe ser nula ",St );
 assertEquals( "Size debe ser 3", 3, s);
 
 setupEscenario2( );
 int y= intt.Size();
 assertNotNull( "No debe ser nula ",intt );
 assertEquals( "Size debe ser 6", 6, y);
 	        	              }


public void testPush()
{ setupEscenario1( );
St.push(d);
int k= St.Size();
assertEquals( "Size debe ser 4", 4, k);

setupEscenario2();
intt.push(55);
int j= intt.Size();
assertEquals( "Size debe ser 7", 7, j);

	
}
public void testpop()
{ setupEscenario1( );
E es=St.pop();

int k= St.Size();
assertEquals( "Size debe ser 2", 2, k);
assertEquals( "Debe ser d",d , es);

setupEscenario2();
int nk=intt.pop();
int j= intt.Size();
assertEquals( "Size debe ser 5", 5, j);
assertEquals( "Debe ser n",6 , nk);

intt.push(99);
int p=intt.pop();
assertEquals( "Debe ser 99",99 , p);

	
}

}